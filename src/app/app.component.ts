import {Component, ViewChild} from '@angular/core';

import {MenuController, Nav, Platform} from 'ionic-angular';


import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {HttpClient} from '@angular/common/http';
import {RootPage} from "../pages/core/root/root";
import{LoginPage}from "../pages/core/login/login"
import { ProductListPage } from '../pages/product/product-list/product-list';

@Component({
  templateUrl: 'app.html'
})
export class AppComponent {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage = LoginPage;
  pages: Array<{ title: string, component: any }>;


  constructor(public platform: Platform,
              public menu: MenuController,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public httpClient: HttpClient) {

    this.initializeApp();

    // set our app's pages
    this.pages = [
      {title: 'Home', component: RootPage},
      {title: 'Store Front', component: ProductListPage},
      {title: 'Settings', component: RootPage},
      {title: 'Logout', component: RootPage},
    ];


  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleBlackOpaque();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}
