import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';
import { environment } from '@app/env';
import { enableProdMode } from '../../node_modules/@angular/core';

if (environment.production) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
    .then((success) => {
        console.log(`Application started`);
        console.log(`environment=${environment.mode}`);
    })
    .catch((err) => console.error(err));




