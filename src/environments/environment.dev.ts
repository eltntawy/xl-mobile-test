import { Environment } from "./environment.model";

export const environment: Environment = {
  mode: 'Dev',
  production: false
}