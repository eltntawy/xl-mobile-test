export interface Environment {
    mode: string;
    production: boolean;
  }