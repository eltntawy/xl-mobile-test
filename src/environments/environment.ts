import { Environment } from "./environment.model";

export const environment: Environment = {
  mode: 'Prod',
  production: true
}