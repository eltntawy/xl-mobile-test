import {Injectable} from '@angular/core';
import {RestService} from './rest-service';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import {AuthorizedModel, OTPCodeModel, ResponseEnity} from '../model/model.interface';

@Injectable()
export class AuthService extends RestService {

    auth(code: OTPCodeModel): Observable<boolean> {
        return this.post(this.AUTH_KEY, code).map(
            (responseEnity: ResponseEnity<AuthorizedModel>) => {
                if (responseEnity.result.isAuthorized) {
                    return true;
                } else {
                    return false;
                }
            });
    }
}
