import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ResponseEnity} from "../model/model.interface";
import {Injectable} from "@angular/core";

@Injectable()
export class RestService {


    private method = "http"
    private ip = "127.0.0.1";
    private port = "5353";

    // services keys and paths need more refinment

    // product services
    public PRODUCTS_KEY = 'products';
    private productPath = '/v1/products/';

    // auth services
    public AUTH_KEY = 'auth';
    private authPath = '/v1/auth/'

    path: Map<string, string> = new Map();

    constructor(private httpClient: HttpClient) {
        this.path.set(this.PRODUCTS_KEY, this.productPath);
        this.path.set(this.AUTH_KEY, this.authPath);

    }

    private getUrlFromKey(key: string): string {
        const uri = this.path.get(key);
        const url = this.method + '://' + this.ip + ':' + this.port + uri;
        console.debug('service url:', url);
        return url;
    }

    get(key: string): Observable<ResponseEnity<any>> | Observable<any> {
        const url = this.getUrlFromKey(key);
        return this.httpClient.get(url);
    }

    post(key: string, body: any): Observable<ResponseEnity<any>> | Observable<any> {
        const url = this.getUrlFromKey(key);
        return this.httpClient.post(url, body);
    }
}
