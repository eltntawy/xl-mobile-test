import {Injectable} from '@angular/core';
import {RestService} from './rest-service';
import {Observable} from 'rxjs';
import {Product, ResponseEnity} from '../model/model.interface';

@Injectable()
export class ProductService extends RestService {

    getAllProducts(): Observable<ResponseEnity<Product []>> {
        return this.get(this.PRODUCTS_KEY);
    }
}
