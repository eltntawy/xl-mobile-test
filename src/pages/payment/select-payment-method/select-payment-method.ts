import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ConfirmPaymentPage} from "../confirm-payment/confirm-payment";

/**
 * Generated class for the SelectPaymentMethodPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-select-payment-method',
  templateUrl: 'select-payment-method.html',
})
export class SelectPaymentMethodPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectPaymentMethodPage');
  }

  confirmPayment() {
    this.navCtrl.push(ConfirmPaymentPage);
  }
}
