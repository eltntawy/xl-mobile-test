import {Component} from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {DonePaymentPage} from '../done-payment/done-payment';

/**
 * Generated class for the ConfirmPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-confirm-payment',
  templateUrl: 'confirm-payment.html',
})
export class ConfirmPaymentPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmPaymentPage');
  }

  payForProduct() {
    let loading = this.loadingCtrl.create({
      spinner: 'show',
      content: `
      <div class="custom-spinner-container">
        <div class="custom-spinner-box"></div>
        <div><span>Memproses  Pembayaran</span></div>
      </div>`,
      duration: 1000
    });

    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
      this.navCtrl.push(DonePaymentPage, {
        isSuccess: true
      });
    });

    loading.present();
  }
}
