import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// Pages
import { HomePage } from '../../core/home/home';
import { SelectPaymentMethodPage } from '../select-payment-method/select-payment-method';

@Component({
    selector: 'page-done-payment',
    templateUrl: 'done-payment.html'
})
export class DonePaymentPage {
    contents: any;
    content: any;

    constructor(
        private navCtrl: NavController,
        private navParams: NavParams
    ) {
        this.contents = {
            success: {
                icon: 'icon checkmark',
                message: 'Terima kasih',
                text: 'Pembayaran sukses dan Paket Xtra Combo 10 GB, Rp 49.000 Anda telah aktif. Silahkan lakukan pengecekan di Halaman Utama',
                button: 'KEMBALI KE HALAMAN UTAMA',
                nextPage: HomePage
            },
            error: {
                icon: 'icon errormark',
                message: 'Pembayaran Gagal',
                text: 'Mohon maaf, pembayaran Paket Anda tidak berhasil. Silahkan coba beberapa saat lagi',
                button: 'PILIH METODE PEMBAYARAN LAIN',
                nextPage: SelectPaymentMethodPage
            }
        }
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DonePaymentPage');
        this.navParams.get('isSuccess') ? this.content = this.contents['success'] : this.content = this.contents['error'];
    }

    back() {
        this.navParams.get('isSuccess') ? this.navCtrl.popToRoot() : this.navCtrl.popTo(SelectPaymentMethodPage);
    }

}