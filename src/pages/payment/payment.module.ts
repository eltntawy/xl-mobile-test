import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ConfirmPaymentPage} from './confirm-payment/confirm-payment';
import {SelectPaymentMethodPage} from "./select-payment-method/select-payment-method";
import {DonePaymentPage} from './done-payment/done-payment';

@NgModule({
  declarations: [
    SelectPaymentMethodPage,
    ConfirmPaymentPage,
    DonePaymentPage
  ],
  imports: [
    IonicPageModule.forChild(SelectPaymentMethodPage),
    IonicPageModule.forChild(ConfirmPaymentPage),
    IonicPageModule.forChild(DonePaymentPage)
  ],
  entryComponents: [
    SelectPaymentMethodPage,
    ConfirmPaymentPage,
    DonePaymentPage
  ]
})
export class PaymentModule {

}
