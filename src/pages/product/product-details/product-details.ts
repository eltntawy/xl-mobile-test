import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {SelectPaymentMethodPage} from "../../payment/select-payment-method/select-payment-method";

/**
 * Generated class for the ProductDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetailsPage');
  }


  buyProduct() {
    this.navCtrl.push(SelectPaymentMethodPage);
  }

}
