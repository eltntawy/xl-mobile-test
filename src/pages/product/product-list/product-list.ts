import {Component, OnInit} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ProductService} from '../../../services/product-service';
import {Product, ResponseEnity, SubProduct} from '../../../model/model.interface';
import {ProductDetailsPage} from "../product-details/product-details";


@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html'
})
export class ProductListPage implements OnInit {
  products: Product[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private productService: ProductService) {
  }

  ngOnInit() {
    this.productService.getAllProducts().subscribe(
      (responseEntity: ResponseEnity<Product[]>) => {
        this.products = responseEntity.result;
      },
      (error: any) => {
        console.debug(error);
      }
    );
  }


  buyThisProduct(product: SubProduct) {
    this.navCtrl.push(ProductDetailsPage,{product: product});
  }

}
