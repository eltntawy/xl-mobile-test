import {ModuleWithProviders, NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ProductDetailsPage} from './product-details/product-details';
import {ProductListPage} from "./product-list/product-list";
import {ProductService} from "../../services/product-service";

@NgModule({
  declarations: [
    ProductDetailsPage,
    ProductListPage
  ],
  imports: [
    IonicPageModule.forChild(ProductDetailsPage),
    IonicPageModule.forChild(ProductListPage),

  ],
  entryComponents: [
    ProductDetailsPage,
    ProductListPage],
  exports: [ProductListPage]
})
export class ProductsModule {

  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: ProductsModule,
      providers: [ProductService]
    };
  }
}
