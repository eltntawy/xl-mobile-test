import {ModuleWithProviders, NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {LoginPage} from './login/login';
import {HomePage} from "./home/home";
import {AuthService} from "../../services/auth-service";
import {RootPage} from "./root/root";

@NgModule({
  declarations: [
    LoginPage,
    HomePage,
    RootPage
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    IonicPageModule.forChild(HomePage),
    IonicPageModule.forChild(RootPage),
  ],
  entryComponents: [
    RootPage,
    HomePage,
    LoginPage
  ]
})
export class CoreModule {

  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [AuthService]
    }
  }
}
