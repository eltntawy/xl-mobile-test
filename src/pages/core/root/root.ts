import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {HomePage} from "../home/home";
import {ProductListPage} from "../../product/product-list/product-list";

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'root-home',
  templateUrl: 'root.html',
})
export class RootPage {

  usageTab = HomePage;
  productsTab = ProductListPage;
  homeTab = HomePage;
  profileTab = HomePage;
  settingsTab = HomePage;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

}
