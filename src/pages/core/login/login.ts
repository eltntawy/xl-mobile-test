import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
// import {ProductListPage} from '../../product/product-list/product-list';
// import {AuthService} from '../../../services/auth-service';
//import {OTPCodeModel} from '../../../model/model.interface';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {RootPage} from "../../../pages/core/root/root";


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  signinform: FormGroup;
  userData = { "username": "", "password": "", "email": ""};

  constructor(public navCtrl: NavController) {
  }


  ngOnInit() {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.signinform = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(10)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]),
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
    });
  }
  
  login() {
    this.navCtrl.push(RootPage);
  }

  // otpModel: OTPCodeModel = { code: '' };
  // authFail: boolean = false;

  // constructor(public navCtrl: NavController, public authService: AuthService) {
  // }

  // login() {

  //   this.authFail = false;
  //   if (this.otpModel.code) {
  //     this.authService.auth(this.otpModel).subscribe(result => {
  //       if (result) {
  //         this.navCtrl.push(ProductListPage, { opt: this.otpModel });
  //       } else {
  //         this.authFail = true;
  //       }
  //     });
  //   } else {
  //     this.authFail = true;

  //   }

  // }
}
