export interface Product {
  internet: [SubProduct]
  sms: [SubProduct]
  call: [SubProduct]
}

export interface SubProduct {
  name: string,
  description: string,
  price: string
}

export interface ResponseEnity<T> {
  statusCode: number,
  statusMessage: string,
  statusDescription: string,
  result: T
}

export interface AuthorizedModel {
  isAuthorized: boolean
}

export interface OTPCodeModel {
  code: string
}
